import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';

//Zsuzsanna Dianovics
//Animation demo
//October 1, 2020
//Dog chasing ball

//main
void main() => runApp(MyAnimationApp());

//stateful widget
class MyAnimationApp extends StatefulWidget {
  _MyAnimationAppState createState() => _MyAnimationAppState();
}

//call from stateful
class _MyAnimationAppState extends State<MyAnimationApp>
    with TickerProviderStateMixin {

  //variables
  AnimationController controller;
  AnimationController sizeController;
  Animation<double> rotation;
  Animation<double> growth;
  Animation<Offset> translation;
  static const _duration = Duration(seconds: 3);
  Curve forwardCurve = Curves.bounceInOut;
  Curve backwardCurve = Curves.bounceInOut;
  CurvedAnimation curvedAnimation;

  //initial state
  @override
  void initState() {
    super.initState();

    //controllers and animations
    controller = AnimationController(
      duration: _duration,
      vsync: this,
    );
    curvedAnimation = CurvedAnimation(
      parent: controller,
      curve: forwardCurve,
      reverseCurve: backwardCurve,
    );
    rotation = Tween<double>(
      begin: 0,
      end: 2 * 3.14,
    ).animate(curvedAnimation)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        }
      });
    sizeController = AnimationController(
      duration: Duration(seconds: 3),
      vsync: this
    );
    growth = Tween(
      begin: 0.0,
      end: 125.0,
    ).animate(sizeController)
    ..addListener(() { 
      setState(() { });
    })
    ..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        sizeController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        sizeController.forward();
      }
    });
    translation = Tween<Offset>(
      begin: Offset(-1, 0),
      end: Offset(2, 0),
    ).animate(curvedAnimation)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
  }


  //build
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Starting',
      home: Scaffold(
        backgroundColor: Colors.green,
        appBar: AppBar(
          title: Text('Play With Me Animation'),
        ),
        body: Column(
          children: [
            SizedBox(height: 25.0),
            //button row
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                FlatButton(
                  color: Colors.green,
                  onPressed: () {
                    controller.forward();
                    sizeController.forward();
                  },
                  child: Text("play", style: TextStyle(fontSize: 15),),
                ),
                FlatButton(
                  color: Colors.green,
                  onPressed: () {
                    controller.stop();
                    sizeController.stop();
                  },
                  child: Text("stop", style: TextStyle(fontSize: 15),),
                )
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            //Clouds
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(child: Image.asset("assets/cloud.png", height: 75,),
                ),
              ],
            ),
          //Trees
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(child: Image.asset("assets/tree.png", height: 50,),
                ),
                Container(child: Image.asset("assets/tree.png", height: 100,),
                ),
              ],
            ),
            //BALL
            Transform.rotate(
              angle: rotation.value,
              child: Center(
                child: Container(
                  child: Image.asset(
                    "assets/ball.png",
                    height:  growth.value,
                    width:  growth.value,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            //DOG
            FractionalTranslation(
              translation: translation.value,
              child: Container(
                  child: Image.asset(
                "assets/dog.png",
                height: 10 + growth.value,
                width: 10 + growth.value,
              )),
            ),
          ],
        ),
      ),
    );
  }
}
